/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_parser.h                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/02 01:16:32 by garm              #+#    #+#             */
/*   Updated: 2014/01/02 01:31:16 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_PARSER_H
# define LIBFT_PARSER_H

/*
** Args Parsing
*/
t_dlist		*ft_lsd_parse_args(int argc, char **argv);

#endif /* LIBFT_PARSER_H */

