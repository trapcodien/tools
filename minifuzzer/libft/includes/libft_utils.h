/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   libft_utils.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/01/02 01:33:42 by garm              #+#    #+#             */
/*   Updated: 2014/01/02 03:45:06 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef LIBFT_UTILS_H
# define LIBFT_UTILS_H

int		*ft_gen_rand_array(int nb, int min, int max);
char	*ft_keygen(size_t len, char min, char max);

#endif /* !LIBFT_UTILS_H */

