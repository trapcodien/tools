/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/18 01:11:36 by garm              #+#    #+#             */
/*   Updated: 2014/02/18 04:55:16 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

# include "ft_input.h"

# define BUF		BUFF_SIZE
# define CLEAN		-1
# define THIS_CRAP	NULL

#endif /* !GET_NEXT_LINE_H */

