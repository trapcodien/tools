/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_input.h                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: garm <garm@student.42.fr>                  +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2014/02/14 21:41:35 by garm              #+#    #+#             */
/*   Updated: 2014/02/18 05:08:09 by garm             ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_INPUT_H
# define FT_INPUT_H

# define BUFF_SIZE 128

# include "get_next_line.h"

typedef struct	s_gnl
{
	int				fd;
	char			*memory;
	struct s_gnl	*next;
}				t_gnl;

int		get_next_line(int const fd, char **line);
char	*get_all_lines(int fd);

#endif /* !FT_INPUT_H */

