#include <stdlib.h>
#include "libft.h"

void	ft_up_to_left(char *base, int size, int index);
char	*ft_fuzz(char *base, int size, int index);

void	ft_up_to_left(char *base, int size, int index)
{
	if (base[index] == '~')
		base[index] = '!';
	if (base[index + 1] == '~')
		ft_up_to_left(base, size, index - 1);
}

char	*ft_fuzz(char *base, int size, int index)
{
	if (index < size)
	{
		if (base[index] == '~')
		{
			ft_up_to_left(base, size, index);
			base = ft_fuzz(base, size, index + 1);
		}
		base[index]++;
	}
	return (base);
}

int		main(int argc, char **argv)
{
	char	*lolilol;
	char	*ref;
	int		size;

	if (argc == 2)
	{
		size = ft_atoi(argv[1]);
		lolilol = ft_strnew(size + 1);
		ref = ft_strnew(size + 1);
		lolilol = (char *)ft_memset((void *)lolilol, '!', size);
		ref = (char *)ft_memset((void *)ref, '~', size);
		ft_putendl(lolilol);
		while (ft_strcmp(ft_fuzz(lolilol, size, 0), ref) != 0)
			ft_putendl(lolilol);
		ft_putendl(lolilol);
		ft_strdel(&lolilol);
	}
	else
	{
		ft_putendl_fd("Usage: ./minifuzzer n", 2);
		exit(1);
	}
	return (0);
}
