#!/usr/bin/php
<?php
	$usage = "Usage: $argv[0] string";
	array_shift($argv);
	if (empty($argv))
		print $usage;
	else
	{
		$len = strlen($argv[0]);
		print "<script>document.write(String.fromCharCode(";
		$str = str_split($argv[0]);
		foreach ($str as $i => $char)
		{
			print ord($char);
			if ($i+1 != $len)
				print ",";
		}
		print "))</script>";
	}

	print "\n";
?>
